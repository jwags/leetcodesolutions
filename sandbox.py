#flip number without converting to string
num = 200 
newNum = 0
while num != 0:
    lastDigit = num % 10
    newNum = (newNum*10) + lastDigit
    num = int(num/10)

print(newNum)
#end


s = "   a   b   c    d   "
x = []
for idx, val in enumerate(s):
    if val != ' ':
        x += val

print(x)