"""
ZigZag Conversion
The string "PAYPALISHIRING" is written in a zigzag pattern on a given number of rows like this: (you may want to display this pattern in a fixed font for better legibility)

P   A   H   N
A P L S I I G
Y   I   R
And then read line by line: "PAHNAPLSIIGYIR"

Write the code that will take a string and make this conversion given a number of rows:

string convert(string s, int numRows);
 

Example 1:

Input: s = "PAYPALISHIRING", numRows = 3
Output: "PAHNAPLSIIGYIR"
Example 2:

Input: s = "PAYPALISHIRING", numRows = 4
Output: "PINALSIGYAHRPI"
Explanation:
P     I    N
A   L S  I G
Y A   H R
P     I
Example 3:

Input: s = "A", numRows = 1
Output: "A"
 

Constraints:

1 <= s.length <= 1000
s consists of English letters (lower-case and upper-case), ',' and '.'.
1 <= numRows <= 1000
"""

#Faster than 90.69%
#Memory usage is less than 71.37%
def solution1(s: str, numRows: int) -> str:
    if numRows == 1:
        return s
    
    output = ""
    characterSkip = (numRows - 1) * 2
    
    # grab first row
    j = 0
    while j < len(s):
        output += s[j]
        j += characterSkip
        
    #grab middle rows
    if numRows > 2:
        for currentRow in range(1, numRows - 1):
            useFirstOffset = True
            firstOffset = characterSkip - (currentRow*2)
            secondOffset = characterSkip - firstOffset
            j = currentRow
            while j < len(s):
                output += s[j]
                j += firstOffset if useFirstOffset else secondOffset
                useFirstOffset = False if useFirstOffset else True
    
    # grab last row
    j = numRows - 1
    while j < len(s):
        output += s[j]
        j += characterSkip
    
    return output

tests = [
    ["PAYPALISHIRING", 3, "PAHNAPLSIIGYIR"],
    ["PAYPALISHIRING", 4, "PINALSIGYAHRPI"],
    ["A", 1, "A"]
    ]

endColor = '\033[0m'
print("")
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0], val[1])
    print
    color = '\033[92m' if actual in val[2] else '\033[91m'
    resultStr = 'pass' if actual in val[2] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}")

"""
Notes
======
p  i   n
a ls  ig
ya h r
p  i

4 rows
6 characters


p   h
a  si
y i r
pl  ig
a   n

5 rows
8 characters


1    1    1
2   02   02 #8,2
3  9 3  9 3 #6,4
4 8  4 8  4 #4,6
57   57   5 #2,8
6    6    6

6 rows
10 characyers


3 rows
4 characters

2 rows
2 characters

1 row
1 characters
"""