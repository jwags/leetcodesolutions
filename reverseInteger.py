"""
7. Reverse Integer

Given a signed 32-bit integer x, return x with its digits reversed. If reversing x causes the value to go outside the signed 32-bit integer range [-231, 231 - 1], then return 0.

Assume the environment does not allow you to store 64-bit integers (signed or unsigned).

 

Example 1:

Input: x = 123
Output: 321
Example 2:

Input: x = -123
Output: -321
Example 3:

Input: x = 120
Output: 21
Example 4:

Input: x = 0
Output: 0
 

Constraints:

-231 <= x <= 231 - 1
"""

#Faster than 18.16%
#Memory usage is less than 13.61%
def solution1(x: int) -> int:
    maxInt = pow(2, 31) - 1
    minInt = pow(2, 31) * -1
    s = str(x)
    result = 0
    if x < 0:
        result = int("-" + s[1:len(s)][::-1])
    else:
        result = int(s[::-1]) 
    return result if result < maxInt and result > minInt else 0

def solution2(x):
    maxInt = pow(2, 31) - 1
    minInt = pow(2, 31) * -1
    newNum = 0
    while x != 0:
        lastDigit = x % 10
        newNum = (newNum*10) + lastDigit
        x = int(x/10)
    return newNum if newNum < maxInt and newNum > minInt else 0

tests = [
    [123, 321],
    [-123, -321],
    [120, 21],
    [0, 0],
    [1534236469, 0]
    ]

endColor = '\033[0m'
print("")
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0])
    print
    color = '\033[92m' if actual == val[1] else '\033[91m'
    resultStr = 'pass' if actual == val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}")

print("")
print("Solution 2")
for idx, val in enumerate(tests):
    actual = solution2(val[0])
    print
    color = '\033[92m' if actual == val[1] else '\033[91m'
    resultStr = 'pass' if actual == val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}")