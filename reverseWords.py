"""
MS Question
===========
151. Reverse Words in a String

Given an input string s, reverse the order of the words.

A word is defined as a sequence of non-space characters. The words in s will be separated by at least one space.

Return a string of the words in reverse order concatenated by a single space.

Note that s may contain leading or trailing spaces or multiple spaces between two words. The returned string should only have a single space separating the words. Do not include any extra spaces.

 

Example 1:

Input: s = "the sky is blue"
Output: "blue is sky the"
Example 2:

Input: s = "  hello world  "
Output: "world hello"
Explanation: Your reversed string should not contain leading or trailing spaces.
Example 3:

Input: s = "a good   example"
Output: "example good a"
Explanation: You need to reduce multiple spaces between two words to a single space in the reversed string.
Example 4:

Input: s = "  Bob    Loves  Alice   "
Output: "Alice Loves Bob"
Example 5:

Input: s = "Alice does not even like bob"
Output: "bob like even not does Alice"
 

Constraints:

1 <= s.length <= 104
s contains English letters (upper-case and lower-case), digits, and spaces ' '.
There is at least one word in s.
 

Follow up:

Could you solve it in-place with O(1) extra space?
"""
#Faster than 89.93%
#Memory usage is less than 43.58%
def solution1(s: str) -> str:
    x = list(filter(None, s.strip().split(' ')))
    return ' '.join(x[::-1])

tests = [
    ["the sky is blue", "blue is sky the"],
    ["  hello world  ", "world hello"],
    ["a good   example", "example good a"],
    ["  Bob    Loves  Alice   ", "Alice Loves Bob"],
    ["Alice does not even like bob", "bob like even not does Alice"],
    ]

endColor = '\033[0m'
print("")
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0])
    print
    color = '\033[92m' if actual == val[1] else '\033[91m'
    resultStr = 'pass' if actual == val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}{(' == ' if actual == val[1] else ' != ')}{val[1]}")