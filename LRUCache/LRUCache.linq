<Query Kind="Program">
  <NuGetReference>Rock.Core.Newtonsoft</NuGetReference>
  <Namespace>Newtonsoft</Namespace>
  <Namespace>Newtonsoft.Json</Namespace>
</Query>

void Main()
{
	var x = new LRUCache(2048);
    string[] lines = System.IO.File.ReadAllLines(@".\input2048.txt");
    var operations = JsonConvert.DeserializeObject(lines[0]);
    var values = JsonConvert.DeserializeObject(lines[1]);
    var capacity = values.RemoveAt(0);
    Console.WriteLine("Hello World!");
}

public class LRUCache {
    private IList<CacheItem> Cache { get; set; }
    private int Capacity { get; set; }

    public LRUCache(int capacity) {
        Cache = new List<CacheItem>();
        Capacity = capacity;
    }
    
    public int Get(int key) {
        // Find item
        //var index = Cache.Select(x => x.Key).IndexOf(key);
        //var index = Cache.Select(x => x.Key).ToList().IndexOf(key);
        var index = Cache.TakeWhile(x => x.Key != key).Count();
        index = index >= 0 && index < Cache.Count ? index : -1;
        if (index < 0)
        {
            // Item not found
            return -1;
        }
        
        // Item found, update date and return
        var matchedItem = Cache.ElementAt(index);
        var updatedItem = new CacheItem
        {
            Key = matchedItem.Key,
            Value = matchedItem.Value,
            // LastUsed = DateTime.Now,
        };
        Cache.RemoveAt(index);
        Cache.Add(updatedItem);
        return matchedItem.Value;
    }
    
    public void Put(int key, int value) {
        // Update existing key
        //var index = Cache.Select(x => x.Key).ToList().IndexOf(key);
        var index = Cache.TakeWhile(x => x.Key != key).Count();
        index = index >= 0 && index < Cache.Count ? index : -1;
        //var index = Cache.FindIndex(x => x.Key == key);
        //var matchedItem = Cache.FirstOrDefault(x => x.Key == key);
        //if (matchedItem != null)
        if (index >= 0)
        {
            Cache.RemoveAt(index);
            Cache.Add(new CacheItem
                      {
                          Key = key,
                          Value = value,
                          // LastUsed = DateTime.Now,
                      });
            // matchedItem.Value = value;
            // matchedItem.LastUsed = DateTime.Now;
            return;
        }
        
        // If at acapacity, remove LRU item
        if (Cache.Count >= Capacity)
        {
            // Cache = Cache.OrderBy(x => x.LastUsed).ToList();
            Cache.RemoveAt(0);
        }
        
        // Add new item
        Cache.Add(new CacheItem
                  {
                      Key = key,
                      Value = value,
                      // LastUsed = DateTime.Now,
                  });
    }
    
    class CacheItem
    {
        public int Key { get; set; }
        public int Value { get; set; }
        // public DateTime LastUsed { get; set; }
    }
}

/**
 * Your LRUCache object will be instantiated and called as such:
 * LRUCache obj = new LRUCache(capacity);
 * int param_1 = obj.Get(key);
 * obj.Put(key,value);
 */

// You can define other methods, fields, classes and namespaces here
/*
Input
["LRUCache","put","put","get","put","put","get"]
[[2],[2,1],[2,2],[2],[1,1],[4,1],[2]]

Expected
[null,null,null,2,null,null,-1]


Input
["LRUCache","put","put","get","put","get","put","get","get","get"]
[[2],[1,1],[2,2],[1],[3,0],[2],[4,4],[1],[3],[4]]

Expected
[null,null,null,1,null,-1,null,-1,0,4]
*/