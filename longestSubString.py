#Faster than 37.84%
#Memory usage is less than 97.63%
def solution5(s):
    lenOfUniqueCharacters = len(set(s))
    lengthOfS = len(s)
    if lengthOfS == lenOfUniqueCharacters:
        return lengthOfS
    longestSubstringLen = 1 if lengthOfS > 0 else 0
    for idx1, val1 in enumerate(s):
        if lenOfUniqueCharacters <= longestSubstringLen:
            break
        if longestSubstringLen > lengthOfS - idx1:
            break
        substring = val1
        for idx2, val2 in enumerate(s[idx1+1:]):
            idx2 = idx1 + idx2 + 1
            if val2 in substring:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
                break
            substring = substring + val2
            if idx2 == lengthOfS - 1:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
    return longestSubstringLen

#Faster than 39.80%
#Memory usage is less than 73.67%
def solution4(s):
    lenOfUniqueCharacters = len(set(s))
    lengthOfS = len(s)
    longestSubstringLen = 1 if lengthOfS > 0 else 0
    for idx1, val1 in enumerate(s):
        if lenOfUniqueCharacters <= longestSubstringLen:
            break
        if longestSubstringLen > lengthOfS - idx1:
            break
        substring = val1
        for idx2, val2 in enumerate(s[idx1+1:]):
            idx2 = idx1 + idx2 + 1
            if val2 in substring:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
                break
            substring = substring + val2
            if idx2 == lengthOfS - 1:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
    return longestSubstringLen

#Fails
def solution3(s):
    lengthOfS = len(s)
    longestSubstringLen = 1 if lengthOfS > 0 else 0
    nextIteration = 0
    for x in range(0, lengthOfS - 1):
        if x < nextIteration:
            continue
        substring = s[x]
        for y in range(x + 1, lengthOfS):
            if s[y] in substring:
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= len(substring) else len(substring)
                nextIteration = y
                break
            substring = substring + s[y]
            if y == lengthOfS - 1:
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= len(substring) else len(substring)
                nextIteration = x + 1
                
    return longestSubstringLen

#Faster than 33.42%
#Memory usage is less than 73.67%
def solution2(s):
    lenOfUniqueCharacters = len(set(s))
    lengthOfS = len(s)
    longestSubstringLen = 1 if lengthOfS > 0 else 0
    for idx1, val1 in enumerate(s):
        if lenOfUniqueCharacters <= longestSubstringLen:
            break
        substring = val1
        for idx2, val2 in enumerate(s[idx1+1:]):
            idx2 = idx1 + idx2 + 1
            if val2 in substring:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
                break
            substring = substring + val2
            if idx2 == lengthOfS - 1:
                lengthOfSubstring = len(substring)
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= lengthOfSubstring else lengthOfSubstring
    return longestSubstringLen

#Faster than 5.01%
#Memory usage is less than 7.21%
def solution1(s):
    lengthOfS = len(s)
    longestSubstringLen = 1 if lengthOfS > 0 else 0
    for x in range(0, lengthOfS - 1):
        substring = s[x]
        for y in range(x + 1, lengthOfS):
            if s[y] in substring:
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= len(substring) else len(substring)
                break
            substring = substring + s[y]
            if y == lengthOfS - 1:
                longestSubstringLen = longestSubstringLen if longestSubstringLen >= len(substring) else len(substring)
                
    return longestSubstringLen


tests = [
    ["", 0],
    [" ", 1],
    ["aaaaa", 1],
    ["abcabcaaa", 3],
    ["bwf", 3],
    ["abcdabcde", 5],
    ["dvdf", 3],
    ["BWWKEW", 3],
    ["anviaj", 5],
    ["abacadaeaf", 3]]


endColor = '\033[0m'
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0])
    color = '\033[92m' if val[1] == actual else '\033[91m'
    resultStr = 'pass' if val[1] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 2")
for idx, val in enumerate(tests):
    actual = solution2(val[0])
    color = '\033[92m' if val[1] == actual else '\033[91m'
    resultStr = 'pass' if val[1] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 3")
for idx, val in enumerate(tests):
    actual = solution3(val[0])
    color = '\033[92m' if val[1] == actual else '\033[91m'
    resultStr = 'pass' if val[1] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 4")
for idx, val in enumerate(tests):
    actual = solution4(val[0])
    color = '\033[92m' if val[1] == actual else '\033[91m'
    resultStr = 'pass' if val[1] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 5")
for idx, val in enumerate(tests):
    actual = solution5(val[0])
    color = '\033[92m' if val[1] == actual else '\033[91m'
    resultStr = 'pass' if val[1] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")