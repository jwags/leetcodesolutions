
"""
5. Longest Palindromic Substring - Medium
Given a string s, return the longest palindromic substring in s.

Example 1:

Input: s = "babad"
Output: "bab"
Note: "aba" is also a valid answer.
Example 2:

Input: s = "cbbd"
Output: "bb"
Example 3:

Input: s = "a"
Output: "a"
Example 4:

Input: s = "ac"
Output: "a"
 

Constraints:

1 <= s.length <= 1000
s consist of only digits and English letters (lower-case and/or upper-case),
"""

#Faster than 76.80%
#Memory usage is less than 25.43%
"""
def solution1(s: str) -> str:
    lengthOfS = len(s)
    if lengthOfS == 1:
        return s[0]
    if lengthOfS == 2 and s[0] == s[1]:
        return s[0] + s[1]

    longestPalindrome = s[0]
    for x in range(0, len(s)):
        offsetLeft = -1
        offsetRight = -1
        currentPalindrome = s[x]
        while True:
            if x + offsetLeft >= 0:


    return longestPalindrome
    
    longestPalindrome = s[0]
    for x in range(1, len(s) - 1):
        palandromSubString = ''
        palandromMatching = True
        for y in range(1, x + 1):
            # if even
            leftOffset = y
            # if odd
            if x % 2 == 1:
                leftOffset = y + 1
            # for both
            rightOffset = y + 1
            if s[leftOffset * -1] == s[rightOffset]:
                palandromSubString = s[leftOffset:rightOffset]
            else:
                palandromMatching = False
                if len(longestPalindrome) < len(palandromSubString):
                    longestPalindrome = palandromSubString
                break
                    
    return longestPalindrome
"""

def solution4(s: str) -> str:
    

def solution3(s: str) -> str:
    longestPalindrome = ""
    for idx, val in enumerate(s):
        substring = s[idx:idx + 1]
        longestPalindrome = substring if len(substring) > len(longestPalindrome) and isPalindrome(substring) else longestPalindrome
    return longestPalindrome

#fails for some scenarios or timesout if you remove the jump
def solution2(s: str) -> str:
    if isPalindrome(s):
        return s
    longestPalindrome = ''
    rightOffset = 0
    for idx, val in enumerate(s):
        if idx < rightOffset:
            continue
        currentPalindrome = expandPalindrome(s, idx, idx + 1)
        if len(longestPalindrome) < len(currentPalindrome[0]):
            longestPalindrome = currentPalindrome[0]
            rightOffset = currentPalindrome[1]
    return longestPalindrome

def expandPalindrome(s, leftOffset, rightOffset):
    longestPalindrome = s[leftOffset:rightOffset]
    if leftOffset > 0 and isPalindrome(s[leftOffset - 1:rightOffset]):
        currentPalindrome = expandPalindrome(s, leftOffset - 1, rightOffset)
        if len(longestPalindrome) < len(currentPalindrome[0]):
            longestPalindrome = currentPalindrome[0]
            # rightOffset = currentPalindrome[1]

    if rightOffset < len(s) and isPalindrome(s[leftOffset:rightOffset + 1]):
        currentPalindrome = expandPalindrome(s, leftOffset, rightOffset + 1)
        if len(longestPalindrome) < len(currentPalindrome[0]):
            longestPalindrome = currentPalindrome[0]
            # rightOffset = currentPalindrome[1]

    if leftOffset > 0 and rightOffset < len(s) and isPalindrome(s[leftOffset-1:rightOffset + 1]):
        currentPalindrome = expandPalindrome(s, leftOffset - 1, rightOffset + 1)
        if len(longestPalindrome) < len(currentPalindrome[0]):
            longestPalindrome = currentPalindrome[0]
            # rightOffset = currentPalindrome[1]

    return [longestPalindrome, rightOffset]


def isPalindrome(s):
    if len(s) == 0:
        return False
    passed = True
    i = 0
    j = len(s) - 1
    while True:
        if s[i] != s[j]:
            passed = False
            break
        i += 1
        j -= 1
        if j < i: 
            break

    return passed

tests = [
    ["babad",["bab", "aba"]],
    ["cbbd",["bb"]],
    ["a",["a"]],
    ["ac",["a", "c"]],
    ["aa",["aa"]],
    ["ab",["a", "b"]],
    ["cbabc",["cbabc"]],
    ["cbabd",["bab"]],
    ["cbaabc",["cbaabc"]],
    ["aabbccbbaa", ["aabbccbbaa"]],
    ["aaaaaaaaaaaaa", ["aaaaaaaaaaaaa"]],
    ["daabfgefg",["aa"]],
    ["daa",["aa"]],
    ["",[""]],
    # ["aaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaa", ["aaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaaaaaabbbbbbbbbbccccccccccddddddddddeeeeeeeeeeffffffffffgggggggggghhhhhhhhhhiiiiiiiiiijjjjjjjjjjkkkkkkkkkkllllllllllmmmmmmmmmmnnnnnnnnnnooooooooooppppppppppqqqqqqqqqqrrrrrrrrrrssssssssssttttttttttuuuuuuuuuuvvvvvvvvvvwwwwwwwwwwxxxxxxxxxxyyyyyyyyyyzzzzzzzzzzyyyyyyyyyyxxxxxxxxxxwwwwwwwwwwvvvvvvvvvvuuuuuuuuuuttttttttttssssssssssrrrrrrrrrrqqqqqqqqqqppppppppppoooooooooonnnnnnnnnnmmmmmmmmmmllllllllllkkkkkkkkkkjjjjjjjjjjiiiiiiiiiihhhhhhhhhhggggggggggffffffffffeeeeeeeeeeddddddddddccccccccccbbbbbbbbbbaaaa"]],
    # ["ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggggg", ["ffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffffff"]],
    ["bananas", ["anana"]],
    # ["321012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210123210012321001232100123210123", ["321012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210012321001232100123210123"]],
    ["3210123210012321001232101232100123210123", ["321012321001232100123210123"]]
    ]

endColor = '\033[0m'
# print("Solution 2")
# for idx, val in enumerate(tests):
#     actual = solution2(val[0])
#     print
#     color = '\033[92m' if actual in val[1] else '\033[91m'
#     resultStr = 'pass' if actual in val[1] else 'fail'
#     print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 3")
for idx, val in enumerate(tests):
    actual = solution3(val[0])
    print
    color = '\033[92m' if actual in val[1] else '\033[91m'
    resultStr = 'pass' if actual in val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")