"""
9. Palindrome Number

Given an integer x, return true if x is palindrome integer.

An integer is a palindrome when it reads the same backward as forward. For example, 121 is palindrome while 123 is not.

 

Example 1:

Input: x = 121
Output: true
Example 2:

Input: x = -121
Output: false
Explanation: From left to right, it reads -121. From right to left, it becomes 121-. Therefore it is not a palindrome.
Example 3:

Input: x = 10
Output: false
Explanation: Reads 01 from right to left. Therefore it is not a palindrome.
Example 4:

Input: x = -101
Output: false
 

Constraints:

-231 <= x <= 231 - 1
 

Follow up: Could you solve it without converting the integer to a string?
"""
#Faster than 48.08%
#Memory usage is less than 79.62%
def solution1(x):
    if x < 0 or x > pow(2, 31) - 1:
        return False
    s = str(x)
    i = 0
    j = len(s) - 1
    isPalindrome = True
    while i < j:
        if s[i] != s[j]:
            isPalindrome = False
            break
        i += 1
        j -= 1
    
    return isPalindrome


tests = [
    [-1, False],
    [0, True],
    [121, True],
    [1, True],
    [123, False],
    [pow(2, 31), False]
    ]

endColor = '\033[0m'
print("")
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0])
    print
    color = '\033[92m' if actual == val[1] else '\033[91m'
    resultStr = 'pass' if actual == val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}{(' == ' if actual == val[1] else ' != ')}{val[1]}")
