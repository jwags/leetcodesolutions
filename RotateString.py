"""
MS Question
===========
We are given two strings, A and B.

A shift on A consists of taking string A and moving the leftmost character to the rightmost position. For example, if A = 'abcde', then it will be 'bcdea' after one shift on A. Return True if and only if A can become B after some number of shifts on A.

Example 1:
Input: A = 'abcde', B = 'cdeab'
Output: true

Example 2:
Input: A = 'abcde', B = 'abced'
Output: false
Note:

A and B will have length at most 100.
"""

#Runtime: 16 ms
#Memory usage: 13.5 MB
def solution1(A, B):
    """
    :type A: str
    :type B: str
    :rtype: bool
    """
    if len(A) == 0 and len(B) == 0:
        return True
    newString = A
    match = False
    for idx, val in enumerate(A):
        if newString == B:
            match = True
            break
        newString = newString[1:] + newString[0]
    
    return match

tests = [
    ["","",True],
    ["abcde","cdeab",True],
    ["abcde","abced",False],
    ["aaaaaa","aaa",False]]


endColor = '\033[0m'
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0], val[1])
    color = '\033[92m' if val[2] == actual else '\033[91m'
    resultStr = 'pass' if val[2] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")