# Commands and Syntax Cheat Sheet

## Python

### Lists

---
Reverse a list

    list[::-1]

Input

    [0,1,2,3]

Output

    [3,2,1,0]
---
Join items in array

    ' ',join(list)

Input

    [0,1,2,3]

Output

    "0 1 2 3"
---
Trim whitespace

    s.strip()

Input

    "    a    "

Output

    "a"
---
Set multiple variables

This will assign a the value 1, and b the value 2.

    a, b = 1, 2

This will assign a and b the value 1, because 1 is an int and is immutable. Immutable types are int, float, str.

    a = b = 1

This will assign point a to b, and assign b to the list because lists (and dictionaries) are mutable.

    a = b = [0, 1, 2]
    
    print(a is b)
    # True
    
    a[0] = 100
    print(a)
    # [100, 1, 2]
    
    print(b)
    # [100, 1, 2]