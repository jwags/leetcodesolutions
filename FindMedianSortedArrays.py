#Faster than 95.20%
#Memory usage is less than 25.43%
def solution2(nums1, nums2):
    """
    :type nums1: List[int]
    :type nums2: List[int]
    :rtype: float
    """
    allNumbers = nums1 + nums2
    lenOfUniqueNumbers = len(set(allNumbers))
    if lenOfUniqueNumbers == 1:
        return allNumbers[0]
    allNumbers.sort()
    length = len(allNumbers)
    if length % 2 == 1:
        return allNumbers[int(length/2)]
    
    half = int(length/2)
    num1 = allNumbers[half - 1]
    num2 = allNumbers[half]
    return float(num1 + num2)/2.0

#Faster than 76.80%
#Memory usage is less than 25.43%
def solution1(nums1, nums2):
    """
    :type nums1: List[int]
    :type nums2: List[int]
    :rtype: float
    """
    allNumbers = nums1 + nums2
    allNumbers.sort()
    length = len(allNumbers)
    if length % 2 == 1:
        return allNumbers[int(length/2)]
    
    half = int(length/2)
    num1 = allNumbers[half - 1]
    num2 = allNumbers[half]
    return float(num1 + num2)/2.0
    
tests = [
    [[1],[2,3],2],
    [[1, 3], [2], 2],
    [[1,2], [3,4], 2.5],
    [[0, 0], [0, 0], 0],
    [[], [1], 1],
    [[2], [], 2],
    [[2, 5], [6, 1], 3.5],
    [[0,0,0], [0,0], 0],
    [[0,0,0], [0,0,0], 0]]



endColor = '\033[0m'
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0], val[1])
    color = '\033[92m' if val[2] == actual else '\033[91m'
    resultStr = 'pass' if val[2] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 2")
for idx, val in enumerate(tests):
    actual = solution2(val[0], val[1])
    color = '\033[92m' if val[2] == actual else '\033[91m'
    resultStr = 'pass' if val[2] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")

print("")
print("Solution 3")
for idx, val in enumerate(tests):
    actual = solution3(val[0], val[1])
    color = '\033[92m' if val[2] == actual else '\033[91m'
    resultStr = 'pass' if val[2] == actual else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}")
