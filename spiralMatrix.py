"""
MS Question
===========
54. Spiral Matrix
Given an m x n matrix, return all elements of the matrix in spiral order.

Example 1:
Input: matrix = [[1,2,3],[4,5,6],[7,8,9]]
Output: [1,2,3,6,9,8,7,4,5]

Example 2:
Input: matrix = [[1,2,3,4],[5,6,7,8],[9,10,11,12]]
Output: [1,2,3,4,8,12,11,10,9,5,6,7]
 
Constraints:
m == matrix.length
n == matrix[i].length
1 <= m, n <= 10
-100 <= matrix[i][j] <= 100
"""

#Faster than 85.53%
#Memory usage is less than 62.93%
def solution1(matrix):
    yMax = len(matrix)
    xMax = len(matrix[0])
    xMin = x = y = 0
    yMin = 1
    increaseX = increaseY = True
    result = []
    while True:
        #move right/left
        if increaseX:
            x = xMin
            changeXDirectionAt = xMax
        else:
            x = xMax - 1
            changeXDirectionAt = xMin - 1
        while x != changeXDirectionAt:
            result += [matrix[y][x]]
            x += 1 if increaseX else -1
        if increaseX:
            x -= 1
            xMax -= 1
            increaseX = False
        else:
            x += 1
            xMin += 1
            increaseX = True
        
        # break if at end
        if (increaseY and yMin >= yMax) or (increaseY == False and yMax <= yMin):
            break

        #move up/down
        if increaseY:
            y = yMin
            changeYDirectionAt = yMax
        else:
            y = yMax - 1
            changeYDirectionAt = yMin - 1
        while y != changeYDirectionAt:
            result += [matrix[y][x]]
            y += 1 if increaseY else -1
        if increaseY:
            y -= 1
            yMax -= 1
            increaseY = False
        else:
            y += 1
            yMin += 1
            increaseY = True

        # break if at end
        if (increaseX and xMin >= xMax) or (increaseX == False and xMax <= xMin):
            break
    return result

tests = [
    [[[1,2,3],[4,5,6],[7,8,9]], [1,2,3,6,9,8,7,4,5]],
    [[[1,2,3,4],[5,6,7,8],[9,10,11,12]], [1,2,3,4,8,12,11,10,9,5,6,7]],
]
endColor = '\033[0m'
print("")
print("Solution 1")
for idx, val in enumerate(tests):
    actual = solution1(val[0])
    print
    color = '\033[92m' if actual == val[1] else '\033[91m'
    resultStr = 'pass' if actual == val[1] else 'fail'
    print(f"{color}Test {str(idx)}: {resultStr}{endColor}, {actual}{(' == ' if actual == val[1] else ' != ')}{val[1]}")
