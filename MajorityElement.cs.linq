<Query Kind="Program" />

/*
MS Question
===========
Given an integer array of size n, find all elements that appear more than ⌊ n/3 ⌋ times.

Follow-up: Could you solve the problem in linear time and in O(1) space?

 

Example 1:

Input: nums = [3,2,3]
Output: [3]
Example 2:

Input: nums = [1]
Output: [1]
Example 3:

Input: nums = [1,2]
Output: [1,2]
 

Constraints:

1 <= nums.length <= 5 * 104
-109 <= nums[i] <= 109
*/
void Main()
{
    var nums = new List<int>
    {
        1,2
    };
    var minThreshold = nums.Count() / 3;
    var matches = new List<int>();
    foreach (var x in nums.Distinct())
    {
        if (nums.Count(y => y == x) > minThreshold)
        {
            matches.Add(x);
        }
    }
    
    Console.WriteLine(matches);
}
